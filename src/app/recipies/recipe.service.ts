import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService{

    recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[] = [
        new Recipe('Chicken Garlic Bread','From Dominoes','https://cdn.pixabay.com/photo/2016/06/15/19/09/food-1459693_1280.jpg',[
            new Ingredient('Chicken',1),
            new Ingredient('Bread',2),
            new Ingredient('Garlic',3)
        ]),
        new Recipe('Egg Curry','From Bawarchi','https://i2.wp.com/www.downshiftology.com/wp-content/uploads/2018/12/Shakshuka-19.jpg',[
            new Ingredient('Eggs',10),
            new Ingredient('Tomato',4),
            new Ingredient('Coriander',3)
        ])
      ];
      constructor(private slService: ShoppingListService ){}

      getRecipes(){
          return this.recipes.slice();
      }

      addIngredientsToShoppingList(ingredients: Ingredient[]){
        this.slService.addIngredients(ingredients);
      }
}